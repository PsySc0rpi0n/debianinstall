#!/bin/bash
set -x

function pause(){
   read -p "$*"
}

# Install vim
sudo apt-get install vim -y 

# Create folders for mounting points if they don't already exist
echo -e "Creating folders for mount points\n"
if [ ! -d "$HOME/mnt/tmp" ]; then
    echo "Didn't exist. Creating..."
    mkdir -p "$HOME/mnt/tmp"
fi 
if [ ! -d "$HOME/mnt/usb1" ]; then
 	echo "Didn't exist. Creating..."
 	mkdir -p "$HOME/mnt/usb1"
fi
 
# Update sources.list
pause 'Press [Enter] to continue:'
echo -e "Updating sources.list\n"
echo "deb http://deb.debian.org/debian/ bookworm main contrib non-free-firmware" | sudo tee /etc/apt/sources.list 1>&2
echo -e "deb-src http://deb.debian.org/debian/ bookworm main contrib non-free-firmware\n" | sudo tee -a /etc/apt/sources.list 1>&2
echo "deb http://deb.debian.org/debian/ bookworm-proposed-updates contrib main non-free non-free-firmware" | sudo tee -a /etc/apt/sources.list 1>&2
echo -e "deb-src http://deb.debian.org/debian/ bookworm-proposed-updates contrib main non-free non-free-firmware\n" | sudo tee -a /etc/apt/sources.list 1>&2
echo "deb http://deb.debian.org/debian/ bookworm-backports contrib main non-free-firmware" | sudo tee -a /etc/apt/sources.list 1>&2
echo -e "deb-src http://deb.debian.org/debian/ bookworm-backports contrib main non-free non-free-firmware\n" | sudo tee -a /etc/apt/sources.list 1>&2
echo "deb http://deb.debian.org/debian-security/ bookworm-security contrib main non-free non-free-firmware" | sudo tee -a /etc/apt/sources.list 1>&2
echo -e "deb-src http://deb.debian.org/debian-security/ bookworm-security contrib main non-free non-free-firmware\n" | sudo tee -a /etc/apt/sources.list 1>&2
echo "deb http://deb.debian.org/debian/ bookworm-updates contrib main non-free-firmware" | sudo tee -a /etc/apt/sources.list 1>&2
echo "deb-src http://deb.debian.org/debian/ bookworm-updates contrib main non-free-firmware" | sudo tee -a /etc/apt/sources.list 1>&2
 
# Update sources list and upgrade system with latest packages
echo -e "Update the system lists and upgrade the system...\n"
sudo apt-get update -y && sudo apt-get upgrade -y
echo -e "Update the system with some nedded packages\n"
sudo apt-get install build-essential thunderbird curl nginx git irssi 7zip wget -y
 
# Requitements for refracta snapshot
echo -e "Installing pre-requits for refracta Snapshot\n"
sudo apt install rsync squashfs-tools xorriso live-boot live-config live-boot-initramfs-tools syslinux syslinux-common yad isolinux
 
# Requirements for openOCD
echo -e "Installing pre-requisits for OpenOCD\n"
sudo apt-get install libtool m4 pkg-config libusb-1.0-0-dev libhidapi-dev libftdi-dev libcapstone-dev -y
 
# Create tmp folder to download software
if [ ! -d "$HOME/Downloads/tmp" ]; then
 	mkdir -p "$HOME/Downloads/tmp"
fi
 
# Download jimtcl, openocd, compile and install
cd "$HOME/Downloads/tmp" || exit
git clone https://github.com/msteveb/jimtcl.git
cd jimtcl || exit
./configure
make
 
cd "$HOME/Downloads/tmp" || exit
git clone https://git.code.sf.net/p/openocd/code openocd-code
cd openocd-code || exit
./bootstrap
./configure --enable-verbose-usb-io --enable-verbose-usb-comms --enable-ftdi --enable-stlink --enable-ulink --enable-usb-blaster-2 --enable-ft232r --enable-armjtagew --enable-usbprog --enable-esp-usb-jtag --enable-cmsis-dap --enable-usb-blaster --enable-openjtag --enable-buspirate --enable-jlink --enable-bcm2835gpio --enable-xlnx-pcie-xvc --enable-jimtcl-maintainer --enable-internal-libjaylink --enable-remote-bitbang --with-capstone
make -j"$(nproc --all)"
sudo make install


# Download, install and configure arduino-cli
echo -e "Installing aruino-cli and some configs\n"
if [ ! -d "$HOME/Downloads/tmp/arduino-cli" ]; then
	mkdir -p "$HOME/Downloads/tmp/arduino-cli"
fi
wget https://downloads.arduino.cc/arduino-cli/arduino-cli_latest_Linux_64bit.tar.gz -O "$HOME/Downloads/tmp/arduino-cli/arduino-cli_latest_Linux_64bit.tar.gz"
tar -zxvf "$HOME/Downloads/tmp/arduino-cli/arduino-cli_latest_Linux_64bit.tar.gz" -C "$HOME/Downloads/tmp/arduino-cli"

# Create config file and populate it with core links
cd "$HOME/Downloads/tmp/arduino-cli" || exit
./arduino-cli config init

# Create populated arduino-cli config file
cd "$HOME/mnt/tmp/debianinstall" || exit
cat arduino-cli.yaml > "$HOME/.arduino15/arduino-cli.yaml"
pause 'Press [Enter] to conitnue!'
sed -e "s|worda|$HOME/.arduino15|" -e "s|wordb|$HOME/.arduino15/staging|; t" -e "s|wordc|$HOME/Arduino|; t" < "$HOME/mnt/tmp/debianinstall/arduino-cli.yaml" > "$HOME/.arduino15/arduino-cli.yaml"
pause 'Press [Enter] to conitnue!'

# Download and install important core files
cd "$HOME/Downloads/tmp/arduino-cli" || exit
./arduino-cli core install STMicroelectronics:stm32
./arduino-cli core install esp8266:esp8266
./arduino-cli core install esp32:esp32
./arduino-cli core install arduino:mbed_rp2040

# Install Vim plugins of interest and config file
if [ ! -d "$HOME/.vim/bundle" ]; then
 	mkdir -p "$HOME/.vim/bundle"
fi
 
cd "$HOME/.vim/bundle" || exit
git clone https://github.com/VundleVim/Vundle.vim.git
 
if [ ! -f "$HOME/.vim/vimrc" ]; then
 	cd "$HOME/mnt/tmp/debianinstall" || exit
 	cat vimrc > "$HOME/.vim/vimrc"
fi

# Download and install Arduino IDE nightly build
# Needs manual update
cd "$HOME/Downloads/tmp" || exit
wget https://downloads.arduino.cc/arduino-ide/nightly/arduino-ide_nightly-latest_Linux_64bit.zip
7z x arduino-ide_nightly-latest_Linux_64bit.zip
 
# Download and install ARM Toolchain for Linux x86_64
wget https://developer.arm.com/-/media/Files/downloads/gnu/13.2.rel1/binrel/arm-gnu-toolchain-13.2.rel1-x86_64-arm-none-eabi.tar.xz
tar -xvf arm-gnu-toolchain-13.2.rel1-x86_64-arm-none-eabi.tar.xz
export PATH="$PATH:~/Downloads/tmp/arm-gnu-toolchain-13.2.Rel1-x86_64-arm-none-eabi/bin:~/Downloads/tmp/arm-gnu-toolchain-13.2.Rel1-x86_64-arm-none-eabi/arm-none-eabi/bin"
echo "$PATH" > "$HOME/.bash_profile"
source "$HOME/.bash_profile"

# Download and install Discord ptb version
cd "$HOME/Downloads/tmp" || exit
wget https://dl-ptb.discordapp.net/apps/linux/0.0.65/discord-ptb-0.0.65.deb
sudo dpkg -i discord-ptb-0.0.65.deb

# Install refractasnapshot
cd "$HOME/Downloads/tmp" || exit
wget https://sourceforge.net/projects/refracta/files/tools/refractasnapshot-base_10.2.12_all.deb
wget https://sourceforge.net/projects/refracta/files/tools/refractasnapshot-gui_10.2.12_all.deb
sudo dpkg -i refractasnapshot-base_10.2.12_all.deb
sudo dpkg -i refractasnapshot-gui_10.2.12_all.deb

# Cleaning the system
cd "$HOME/Downloads/tmp" || exit
if [ -f "arduino-cli_latest_Linux_64bit.tar.gz" ]; then
    rm -f "arduino-cli_latest_Linux_64bit.tar.gz"
    # rm -f "$HOME/.arduino15/arduino-cli.yaml"
fi

if [ -f "arduino-ide_nightly-latest_Linux_64bit.zip" ]; then
    rm -f "arduino-ide_nightly-latest_Linux_64bit.zip"
fi

if [ -f "arm-gnu-toolchain-13.2.rel1-x86_64-arm-none-eabi.tar.xz" ]; then
    rm -f "arm-gnu-toolchain-13.2.rel1-x86_64-arm-none-eabi.tar.xz"
fi

if [ -f "refractasnapshot-base_10.2.12_all.deb" ]; then
    rm -f "refractasnapshot-base_10.2.12_all.deb"
fi

if [ -f "refractasnapshot-gui_10.2.12_all.deb" ]; then
    rm -f "refractasnapshot-gui_10.2.12_all.deb"
fi

if [ -f "discord-ptb-0.0.65.deb" ]; then
    rm -f "discord-ptb-0.0.65.deb"
fi

# cd "$HOME/Downloads/tmp/openocd-code" | exit
# make clean
# sudo make uninstall

# sudo apt remove --purge refracta*
# sudo apt autoremove

# Set PATH back to original
# export PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games
