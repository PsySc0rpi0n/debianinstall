# DebianInstall script
Note: This is not to install Debian but to install some packages, plguins and to setup some configurations to work mostly with programming, embedded systems, openOCD, arduino-cli, vim, etc.

# Getting started
```
git clone https://gitlab.com/PsySc0rpi0n/debianinstall.git
./install.sh
```

This will update sources.list with Debian mirrors and will run update and upgrade
After this it will download compile and install pre-requisits for openOCD and also compile and install openOCD.
It will also install Vim and its pre-requisits via apt. After the install is complete, it will take care to install Vundle plugin.
After it finishes, you should be able to start Vim, and run the follwing command inside Vim:
`:PluginInstall`

This will install a couple more plugins to make Vim more programmer user-friendly.

## Note: The configuration of these plugins is not performed with this script

Plugins installed include:
- [ ] [LightLine](https://github.com/itchyny/lightline.vim)
- [ ] [ALEDetail](https://github.com/dense-analysis/ale)
- [ ] [auto-pairs](https://github.com/LunarWatcher/auto-pairs)

